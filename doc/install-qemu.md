# Provisioningp VM on Mac M1 using QEMU and Vagrant

The setup for Mac M1 user.

# Install QEMU on MAC M1

QEMU is a generic and open source machine & userspace emulator and
virtualizer. [see more](https://www.qemu.org/)
There are two ways to install, but you have to make sure hvf is enabled, in some cases hvf won't be enabled if you install via brew

## Build QEMU from source

0. install tools

```bash
$ brew install ninja pkgconfig glib pixman
```

1. Dowload [at](https://download.qemu.org/qemu-8.0.0-rc0.tar.xz).
2. Unzip.
3. Navigate to dir after unzip.
4. To build the build run below commands.

```bash
$ mkdir build && cd build
$ ../configure --enable-cocoa --enable-fdt --disable-kvm --disable-xen --enable-gcrypt --enable-hvf
$ make -j4
```

5. To validate the build run below commands

```bash
./aarch64-softmmu/qemu-system-aarch64 --help
```

6. Add binary to PATH

```bash
$ cp ./qemu-img ~/.local/bin/
$ cp ./aarch64-softmmu/qemu-system-aarch64 ~/.local/bin/
```

## Install QEMU with Brew

```bash
$ brew install qemu
$ ln -s  /opt/homebrew/Cellar/qemu/7.2.0/bin/ /opt/homebrew/share/qemu
```

### Get machine support

```bash
$ qemu-system-aarch64 -machine help
```
