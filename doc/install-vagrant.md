# Setup Vagrant on Mac M1

## Install

Download the latest version of vagrant at https://www.vagrantup.com/Downloads and install it.
After installation, run the following command in your terminal to make sure it installed properly.

```bash
vagrant --version
```

## Install Vagrant QEMU Provider

```bash
vagrant plugin install vagrant-qemu
```

## Set default provider

```bash
echo "export VAGRANT_DEFAULT_PROVIDER=qemu" >> ~/.zshrc
```

## Debug

Serial port is exported to unix socket: `<user_home>/.vagrant.d/tmp/vagrant-qemu/<id>/qemu_socket_serial`, or `debug_port`.

To debug and login to the GuestOS from serial port:

- unix socket
  1. Get the id: `.vagrant/machines/default/qemu/id` in same directory with `Vagrantfile`
  2. Get the path to `qemu_socket_serial`
  3. Use `nc` to connect: `nc -U /Users/.../qemu_socket_serial`
- `debug_port` (for example: 33334)
  - Use `nc` to connect: `nc localhost 33334`

To send ctrl+c to GuestOS from `nc`, try:

- unix socket
  - `echo 03 | xxd -r -p | nc -U /Users/.../qemu_socket_serial`
- `debug_port` (for example: 33334)
  - `echo 03 | xxd -r -p | nc localhost 33334`

## Known issue / Troubleshooting

### 1. failed to create shared folder

```
We couldn't detect an IP address that was routable to this
machine from the guest machine! Please verify networking is properly
setup in the guest machine and that it is able to access this
host.

As another option, you can manually specify an IP for the machine
to mount from using the `smb_host` option to the synced folder.
```

The reason is that the user mode of qemu currently in use does not support ping.
`smb_host` needs to be explicitly specified. For example:

```
Vagrant.configure("2") do |config|
  # ... other stuff

  config.vm.synced_folder ".", "/vagrant", type: "smb", smb_host: "10.0.2.2"
end
```

### 3. Vagrant SMB synced folders require the account password to be stored in an NT compatible format

If you get this error when running `vagrant up`

1. On your M1 Mac, go to System Preferences > Sharing > File Sharing > Options...
2. Tick "Share Files and Folders using SMB"
3. Tick your username
4. Click Done
5. Run `vagrant up` again

### 4. The box you're using with the QEMU provider ('default') is invalid

This may cause by invalid default qemu dir (`/opt/homebrew/share/qemu`).

You can find the correct one by:

```
echo `brew --prefix`/share/qemu
```

And then set it (for example `/usr/local/share/qemu`) in the `Vagrantfile` as:

```
config.vm.provider "qemu" do |qe|
  qe.qemu_dir = "/usr/local/share/qemu"
end
```

### 5. Fix port ssh error

```bash
lsof -i :50022
kill -9 <pid_id>
```
