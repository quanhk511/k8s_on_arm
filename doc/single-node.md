# Single node

Spin up new VM for control plane.

```bash
$ cd spin_vm
$ ./spin_up_control_plan.sh
```

## Ssh to VM

Ssh to VM to perform command setup control plane.

```bash
$ vagrant global-status
$ vagrant ssh control-plane
```

if get error you can do

```bash
$ vagrant ssh <id_of_vm>
```

## Install steps

1. Switch to root

```bash
$ sudo su -
```

2. Navigate to scripts dir

```bash
$ cd /vagrant
```

3. Install tools

```bash
$ ./install_misc.sh && ./install_tools.sh
```

4. Create cert for control plane

```bash
$ ./provision_cert_control_plane.sh
```

5. Bootstrap control plane

```bash
$ ./bootstrap_control_plane.sh
```

6. Check staus

You should have all service in control plane when perform command is following:

```bash
root@control-plane:/~# netstat -nptl
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 192.168.51.10:2380      0.0.0.0:*               LISTEN      9474/etcd
tcp        0      0 192.168.51.10:2379      0.0.0.0:*               LISTEN      9474/etcd
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      598/systemd-resolve
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      760/sshd: /usr/sbin
tcp        0      0 127.0.0.1:2379          0.0.0.0:*               LISTEN      9474/etcd
tcp6       0      0 :::22                   :::*                    LISTEN      760/sshd: /usr/sbin
tcp6       0      0 :::10259                :::*                    LISTEN      9787/kube-scheduler
tcp6       0      0 :::10257                :::*                    LISTEN      9716/kube-controlle
tcp6       0      0 :::6443                 :::*                    LISTEN      9646/kube-apiserver
```

7. Setup kubele and container run time to run workload on control plane

```bash
$ ./provision_cert_kubelet.sh
$ ./bootstrap_kubelet_cr.sh
```

8. Check status kubelet

You should have kubele service in control plane when perform command is following:

```bash
root@control-plane:~# netstat -nptl
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 192.168.51.10:2380      0.0.0.0:*               LISTEN      9474/etcd
tcp        0      0 192.168.51.10:2379      0.0.0.0:*               LISTEN      9474/etcd
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      598/systemd-resolve
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      760/sshd: /usr/sbin
tcp        0      0 127.0.0.1:33317         0.0.0.0:*               LISTEN      10628/containerd
tcp        0      0 127.0.0.1:2379          0.0.0.0:*               LISTEN      9474/etcd
tcp        0      0 127.0.0.1:10248         0.0.0.0:*               LISTEN      10918/kubelet
tcp        0      0 127.0.0.1:10249         0.0.0.0:*               LISTEN      10706/kube-proxy
tcp6       0      0 :::22                   :::*                    LISTEN      760/sshd: /usr/sbin
tcp6       0      0 :::10259                :::*                    LISTEN      9787/kube-scheduler
tcp6       0      0 :::10256                :::*                    LISTEN      10706/kube-proxy
tcp6       0      0 :::10257                :::*                    LISTEN      9716/kube-controlle
tcp6       0      0 :::10250                :::*                    LISTEN      10918/kubelet
tcp6       0      0 :::6443                 :::*                    LISTEN      9646/kube-apiserver
```

Now you can see node when perform command following:

```bash
root@control-plane:~# kubectl get node
NAME            STATUS   ROLES    AGE   VERSION
control-plane   Ready    <none>   21m   v1.23.17
```

## Verify

Create some pod to verify K8s single node.

```bash
$ export KUBECONFIG=/artifacts/cfg/admin.kubeconfig
$ kubectl run alpine --image=alpine:latest --command -- sleep 3600
```

You should have following:

```bash
root@control-plane:~# kubectl get pod
NAME     READY   STATUS    RESTARTS   AGE
alpine   1/1     Running   0          31s
```
