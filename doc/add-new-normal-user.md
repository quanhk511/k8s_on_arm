# Lab hans on certificate signing requests

The Certificates API enables automation of X.509 credential provisioning by providing a programmatic interface for clients of the Kubernetes API to request and obtain X.509 certificates from a Certificate Authority (CA).

A CertificateSigningRequest (CSR) resource is used to request that a certificate be signed by a denoted signer, after which the request may be approved or denied before finally being signed.

## Create CertificateSigningRequest

```bash
$ vagrant ssh
$ sudo su -
$ cd /vagrant/ad-on
$ ./add-new-normal-user.sh
```

## Preparing kubeconfig for the user

Create a kubeconfig file for user and send it to them along with the key and crt file.

```bash
./add-new-normal-user-test.sh
```

## Test the access

```bahs
kubectl --kubeconfig=/artifacts/cert/<user>.config cluster-info
```

And then, you got

```
Error from server (Forbidden): services is forbidden: User "john" cannot list resource "services" in API group "" in the namespace "default"
```

create new role

```bash
kubectl create role developer --verb=create --verb=get --verb=list --verb=update --verb=delete --resource=pods
```

role binding

```bash
kubectl create rolebinding developer-binding-<user> --role=developer --user=<user>
```
