# Lab hands on configure multiple schedulers

Kubernetes ships with a default scheduler that is described here. If the default scheduler does not suit your needs you can implement your own scheduler. Moreover, you can even run multiple schedulers simultaneously alongside the default scheduler and instruct Kubernetes what scheduler to use for each of your pods.

## Define a Kubernetes Deployment for the scheduler

```bash
$ vagrant ssh
$ sudo su -
$ cd /vagrant/add-on
$ ./mul-scheduler.sh
```

If RBAC is enabled on your cluster, you must update the system:kube-scheduler cluster role. Add your scheduler name to the resourceNames of the rule applied for endpoints and leases resources, as in the following example:

```bash
$ kubectl edit clusterrole system:kube-scheduler
```

```yaml
- apiGroups:
    - coordination.k8s.io
  resourceNames:
    - kube-scheduler
    - my-scheduler
```

## Specify schedulers for pods

```bash
$ ./mul-scheduler-test.sh
```
