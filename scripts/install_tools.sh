#!/usr/bin/env bash

set -euxo pipefail

source "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/_variables.sh"

function install_cfssl() {
  if [[ ! -f ~/go/bin/cfssl ]]
  then
      go install github.com/cloudflare/cfssl/cmd/cfssl@latest
  fi

  if [[ ! -f ~/go/bin/cfssljson ]]
  then
      go install github.com/cloudflare/cfssl/cmd/cfssljson@latest
  fi

  chmod +x ~/go/bin/*
}

function install_kubectl() {
  snap install kubectl --classic --channel=1.23/stable
}

function install_helm() {
  snap install helm --classic
}

install_cfssl
install_kubectl
install_helm
