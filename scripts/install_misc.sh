#/bin/bash
function install_misc() {
    apt-get update
    apt-get install -y \
        net-tools jq git golang apt-transport-https ca-certificates
    echo "Install Complted"
}

function setup_shell () {
  cat <<EOF | tee -a ~/.bashrc
# some more kubectl aliases
alias k='kubectl'
alias kd='k describe'
alias kg='k get'
EOF
}

setup_shell
install_misc

