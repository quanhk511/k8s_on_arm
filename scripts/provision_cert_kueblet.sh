#!/usr/bin/env bash

set -euaxo pipefail

source "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/_variables.sh"

function _create_kubelet_certificate() {

  internal_ip=$(hostname -I | awk '{ print $1 }')

  cat > kubelet-${HOSTNAME}-csr.json <<EOF
{
  "CN": "system:node:${HOSTNAME}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "VN",
      "L": "VN",
      "O": "system:nodes",
      "OU": "Platform Engineering",
      "ST": "HCM"
    }
  ]
}
EOF

    ~/go/bin/cfssl gencert \
      -ca=ca.pem \
      -ca-key=ca-key.pem \
      -config=ca-config.json \
      -hostname=${HOSTNAME},${internal_ip} \
      -profile=kubernetes \
      kubelet-${HOSTNAME}-csr.json | ~/go/bin/cfssljson -bare kubelet-${HOSTNAME} \
    && rm -f kubelet-${HOSTNAME}-csr.json kubelet-${HOSTNAME}.csr
}

function _generate_kubelet_configuration() {

  kubectl config set-cluster ${KUBERNETES_CLUSTER_NAME} \
    --certificate-authority=${CERT_DIR}/ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=kubelet.kubeconfig

  kubectl config set-credentials system:node:${HOSTNAME} \
    --client-certificate=${CERT_DIR}/kubelet-${HOSTNAME}.pem \
    --client-key=${CERT_DIR}/kubelet-${HOSTNAME}-key.pem \
    --embed-certs=true \
    --kubeconfig=kubelet.kubeconfig

  kubectl config set-context default \
    --cluster=${KUBERNETES_CLUSTER_NAME} \
    --user=system:node:${HOSTNAME} \
    --kubeconfig=kubelet.kubeconfig

  kubectl config use-context default --kubeconfig=kubelet.kubeconfig
}

function ensure_dir() {

  for _dir in ${ADDON_DIR} ${BASE_DIR} ${CERT_DIR} ${CONFIG_DIR} ${DOWNLOAD_DIR}
  do
    if [[ ! -d ${_dir} ]]
    then
      mkdir -p "${_dir}" && \
      chown -R $(id -u):$(id -g) "${_dir}" && \
      chmod 777 "${_dir}"
    fi
  done
}

ensure_dir

pushd "${CERT_DIR}"
_create_kubelet_certificate
popd

pushd "${CONFIG_DIR}"
_generate_kubelet_configuration
popd
