#!/bin/bash
source "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/../_variables.sh"

function _help() {
  cat <<EOF
    Arguments:
    +\$1 given username
    Usage example:
    $ ./add-new-normal-user-test.sh john
  EOF
}

# The certificate value is in Base64-encoded format under status.certificate.
# Export the issued certificate from the CertificateSigningRequest.
function get_certificate () {
  local USR=$1
  kubectl get csr ${USR} -o jsonpath='{.status.certificate}'| base64 -d > ${USR}.crt
}

function create_kube_config () {
  local USR=$1
  local client_certificate
  local client_key
  local ca

  ca=$(cat ca.pem | base64 | tr -d '\n')
  client_certificate=$(cat ${USR}.crt | base64 | tr -d '\n' )
  client_key=$(cat ${USR}-key.pem | base64 | tr -d '\n' )
  cat > ${USR}.kubeconfig <<EOF
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${ca}
    server: https://127.0.0.1:6443
  name: kubernetes-the-hard-way
contexts:
- context:
    cluster: kubernetes-the-hard-way
    user: $USR
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: $USR
  user:
    client-certificate-data: ${client_certificate}
    client-key-data: ${client_key}

EOF
  cat ${USR}.kubeconfig
  rm -rf ${USR}.crt
  rm -rf ${USR}-key.pem
}

# init vars
USR=$1
if [[ -z $USR ]]; then
  _help
  exit 1
fi

pushd "${CERT_DIR}"
get_certificate "$USR"
create_kube_config "$USR"
popd
