#!/bin/bash
# Even though a normal user cannot be added via an API call, 
# any user that presents a valid certificate signed by the cluster's certificate authority (CA) is considered authenticated. 
# In this configuration, Kubernetes determines the username from the common name field in the 'subject' of the cert (e.g., "/CN=bob"). 
# From there, the role based access control (RBAC) sub-system would determine whether the user is authorized to perform a specific operation on a resource
# X509 Client Certs


source "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/../_variables.sh"

function _help() {
  cat <<EOF
    Arguments:
    +\$1 given username
    Usage example:
    $ ./add-new-normal-user.sh john
  EOF
}

# Create private key
# The following scripts show how to generate PKI private key and CSR. 
# It is important to set CN and O attribute of the CSR. 
# CN is the name of the user and O is the group that this user will belong to. 
# You can refer to RBAC for standard groups.

function _create_user_certificate() {
  local USR=$1
  cat > ${USR}-csr.json <<EOF
{
  "CN": "${USR}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "VN",
      "L": "VN",
      "O": "system:authenticated",
      "OU": "Platform Engineering",
      "ST": "HCM"
    }
  ]
}
EOF

cat ${USR}-csr.json
  ~/go/bin/cfssl genkey "${USR}-csr.json" | ~/go/bin/cfssljson -bare "${USR}" \
  && rm -f ${USR}-csr.json
}

# Create CertificateSigningRequest
# Create a CertificateSigningRequest and submit it to a Kubernetes Cluster via kubectl. 
# Below is a script to generate the CertificateSigningRequest.

function create_certificate_signing_request () {
  local crt_one_line
  local USR=$1
  csr_one_line=$(cat ${USR}.csr | base64 | tr -d "\n")
  cat > ${USR}-cert-request.yaml <<EOF
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: ${USR}
spec:
  request: ${csr_one_line}
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
EOF
  cat ${USR}-cert-request.yaml
  kubectl apply -f ${USR}-cert-request.yaml
  kubectl get csr
  kubectl certificate approve ${USR}
  rm -rf ${USR}-cert-request.yaml
  rm -rf ${USR}.csr
}


# init vars
USR=$1
if [[ -z $USR ]]; then
  _help
  exit 1
fi

pushd "${CERT_DIR}"
_create_user_certificate "$USR"
create_certificate_signing_request "$USR"
popd

