#!/bin/bash
function create_pod_with_my_scheduler () {
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: annotation-second-scheduler
  labels:
    name: multischeduler-example
spec:
  schedulerName: my-scheduler
  containers:
    - name: pod-with-second-annotation-container
      image: docker.io/wodby/nginx
EOF
}

function _verify () {
  echo '''
  # Verifying that the pods were scheduled using the desired schedulers.
  # kubectl get events
  '''
}

create_pod_with_my_scheduler
_verify
