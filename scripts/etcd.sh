#!/usr/bin/env bash

set -euxo pipefail

source "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/_variables.sh"

function setup_compile() {
  apt-get remove binutils-arm-none-eabi gcc-arm-none-eabi
  apt-get install -y gdb-arm-none-eabi
}

function setup_etcd() {
  local etcd_name cluster_name
  etcd_name=$(hostname -s)
  cluster_name="etcd-cluster-0"

  setup_compile

  if [[ ! -d etcd ]]
  then
      git clone -b v${ETCD_VERSION} https://github.com/etcd-io/etcd.git
  fi

  export GOARCH=arm64
  export GOOS=linux
  export GOARM=7
  export GOPATH=${HOME}/go

  cd etcd && ./build.sh && mv bin/etcd* /usr/local/bin/ && chmod a+x /usr/local/bin/etcd* && cd -

  mkdir -p /etc/etcd /var/lib/etcd
  chmod 700 /var/lib/etcd
  cp -f "${CERT_DIR}/ca.pem" "${CERT_DIR}/kubernetes-key.pem" "${CERT_DIR}/kubernetes.pem" /etc/etcd/

  cat <<EOF | tee /etc/systemd/system/etcd.service
  [Unit]
  Description=etcd
  Documentation=https://github.com/coreos

  [Service]
  Environment="ETCD_UNSUPPORTED_ARCH=arm"
  Type=notify
  ExecStart=/usr/local/bin/etcd \\
    --name ${etcd_name} \\
    --cert-file=/etc/etcd/kubernetes.pem \\
    --key-file=/etc/etcd/kubernetes-key.pem \\
    --peer-cert-file=/etc/etcd/kubernetes.pem \\
    --peer-key-file=/etc/etcd/kubernetes-key.pem \\
    --trusted-ca-file=/etc/etcd/ca.pem \\
    --peer-trusted-ca-file=/etc/etcd/ca.pem \\
    --peer-client-cert-auth \\
    --client-cert-auth \\
    --initial-advertise-peer-urls https://${CONTROLLER_INTERNAL_IP}:2380 \\
    --listen-peer-urls https://${CONTROLLER_INTERNAL_IP}:2380 \\
    --listen-client-urls https://${CONTROLLER_INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
    --advertise-client-urls https://${CONTROLLER_INTERNAL_IP}:2379 \\
    --initial-cluster-token ${cluster_name} \\
    --initial-cluster ${etcd_name}=https://${CONTROLLER_INTERNAL_IP}:2380 \\
    --initial-cluster-state new \\
    --data-dir=/var/lib/etcd
  Restart=on-failure
  RestartSec=5

  [Install]
  WantedBy=multi-user.target
EOF

  systemctl daemon-reload
  systemctl enable etcd
  systemctl start etcd
}

function etcd_verify() {
  ETCDCTL_API=3 etcdctl member list \
    --endpoints=https://127.0.0.1:2379 \
    --cacert=/etc/etcd/ca.pem \
    --cert=/etc/etcd/kubernetes.pem \
    --key=/etc/etcd/kubernetes-key.pem
}

pushd "${DOWNLOAD_DIR}"
setup_etcd
sleep 2s
etcd_verify
popd