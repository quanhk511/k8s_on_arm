NETWORK="192.168.51."
WORKER_IP_START=50
NUM_WORKER_NODES=1
CONTROLLER_IP_START = "10"

Vagrant.configure("2") do |config|
  config.vm.define "control-plane" do |node|
    node.vm.hostname = "control-plane"
    node.vm.provider "qemu" do |qe|
      qe.ssh_port = 50022
      qe.memory = "8G"
      qe.machine = "virt-7.2,accel=hvf,highmem=on"
      qe.arch = "aarch64"
      qe.extra_netdev_args = "id=mynet0,net=192.168.51.0/24,dhcpstart=#{NETWORK + CONTROLLER_IP_START},hostname=control-plane"
    end
  end

  config.vm.box = "perk/ubuntu-2204-arm64"
  config.vm.box_version = "20230107"
  config.vm.box_check_update = false
  config.vm.boot_timeout = 6000
  config.vm.synced_folder "scripts", "/vagrant", type: "smb", smb_host: "smb://dmhoangtumhqcom"

  config.vm.provision "shell", run: "once", inline: <<-SHELL
    sudo swapoff -a
    groupadd unpriv_ping
    sudo apt-get purge network-manager
    sysctl -w net.ipv6.conf.all.disable_ipv6=1
    sysctl -w net.ipv6.conf.default.disable_ipv6=1
    sysctl -w net.ipv6.conf.lo.disable_ipv6=1
    sysctl -w net.ipv4.ping_group_range='0 2147483647'
    
    for U in vagrant root ; do
      usermod --append --groups unpriv_ping $U
    done

    (
      GROUP_ID=$(getent group unpriv_ping | cut -f 3 -d :)
      printf 'net.ipv4.ping_group_range = %u %u\n' $GROUP_ID $GROUP_ID \
        >> /etc/sysctl.conf
    )

    sudo sysctl -p

  SHELL

    config.vm.provision "shell", run: "once", inline: <<-SHELL
    sed -i "/^.*control-plane$/d" /etc/hosts
    echo "#{NETWORK}#{CONTROLLER_IP_START} control-plane" >> /etc/hosts

    for i in $(seq 1 #{NUM_WORKER_NODES})
    do
      sed -i "/^.*worker-${i}$/d" /etc/hosts
      echo "#{NETWORK}$((#{WORKER_IP_START}+i))  worker-${i}" >>  /etc/hosts
    done
  SHELL

end
