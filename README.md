# The K8s on ARM project

This repo is my home lab, I take note when I learn how to install K8s from scratch, instead of using the popular kubeadm.

My laptop is Macbook pro M1 pro, you can do the same for other M1 chip Macs.

# Prerequisites

- [Install QEMU](doc/install-qemu.md)
- [Install Vagrant](doc/install-vagrant.md)

## Vagrantfile

The primary function of the Vagrantfile is to describe the type of machine required for a project, and how to configure and provision these machines. Vagrantfiles are called Vagrantfiles because the actual literal filename for the file is Vagrantfile (casing does not matter unless your file system is running in a strict case sensitive mode).

Vagrant is meant to run with one Vagrantfile per project, and the Vagrantfile is supposed to be committed to version control. This allows other developers involved in the project to check out the code, run vagrant up, and be on their way. Vagrantfiles are portable across every platform Vagrant supports.

The syntax of Vagrantfiles is Ruby, but knowledge of the Ruby programming language is not necessary to make modifications to the Vagrantfile, since it is mostly simple variable assignment. In fact, Ruby is not even the most popular community Vagrant is used within, which should help show you that despite not having Ruby knowledge, people are very successful with Vagrant.

I have 3 Vagrantfiles, when Vagrantfile is a common file, declare some variables, script, etc. And I involve the one via `load` function.

```ruby
# Load common
vagrant_root = File.dirname(__FILE__)
common_vagrantfile = "#{vagrant_root}/Vagrantfile"
load common_vagrantfile if File.exists?(common_vagrantfile)
```

# Set up single node

- [Single node](doc/single-node.md)

# Add-on

Set up advance K8s.

- [Set up multiple schedeuler](doc/add-new-scheduler.md)
- [Set up new normal user](doc/add-new-normal-user.md)
- [Set up multiple worker node](doc/add-new-worker-node.md)
