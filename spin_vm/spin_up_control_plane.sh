#!/bin/bash

SH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

function spin_up_control_plane () {
  
  command='vagrant up --provision'
  VM_STATUS=`vagrant status  | grep default | awk '{ print $2 }'`
  case "${VM_STATUS}" in
    running)
       echo "RUNNING"
       vagrant up --provision
    ;;
    poweroff)
       echo "POWEROFF"
       vagrant up --provision
    ;;
    *)
       echo "Unhandled: ${VM_STATUS}"
       vagrant up --provision
    ;;
  esac
}

pushd "${SH}/../"
export VAGRANT_VAGRANTFILE=$(pwd)/Vagrantfile
spin_up_control_plane
popd
